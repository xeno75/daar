package main;

/**
 * Utils
 */
public final class Utils {

    public static enum Color {
        RED, GREEN, CYAN, YELLOW
    }

    public final static String RED = (char) 27 + "[31;1m";
    public final static String GREEN = (char) 27 + "[32;1m";
    public final static String YELLOW = (char) 27 + "[33;1m";
    public final static String CYAN = (char) 27 + "[96;1m";
    public final static String REDBG = (char) 27 + "[41;1m";
    public final static String RESETCOLOR = (char) 27 + "[0m";

    public static int charToint(char c) {
        if ((int) c < 97) {
            return (int) c - (int) 'A' + 26;
        } else {
            return (int) c - (int) 'a';
        }
    }

    public static char intToChar(int i) {
        if (i < 26) {
            return (char) (i + (int) 'a');
        } else {
            return (char) (i + (int) 'A');
        }
    }

    public static boolean isRegEx(String pattern) {
        char[] chars = pattern.toCharArray();
        for (char c : chars) {
            if (c == '+' || c == '*' || c == '|' || c == '.' || c == '\\' || c == '(' || c == ')') {
                return true;
            }
        }
        return false;
    }

    public static boolean isAlpha(String pattern) {
        char[] chars = pattern.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }

    public static String color(String text, Color color) {
        StringBuilder builder = new StringBuilder();
        String c = "";
        switch (color.name()) {
        case "RED":
            c = RED;
            break;
        case "GREEN":
            c = GREEN;
            break;
        case "CYAN":
            c = CYAN;
            break;
        case "YELLOW":
            c = YELLOW;
            break;
        default:
            break;
        }
        builder.append(c).append(text).append(RESETCOLOR);
        return builder.toString();
    }

}