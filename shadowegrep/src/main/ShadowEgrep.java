package main;

import main.Utils.Color;
import main.automaton.Automaton;
import main.kmp.Kmp;
import main.radixtree.RadixTree;

/**
 * ShadowEgrep
 */
public class ShadowEgrep {

    public static void main(String[] args) {

        if (args.length > 1) {

            String pattern = args[0];
            String file = args[1];
            boolean repeat = false;

            System.out.println("\nLooking for  : `" + Utils.color(pattern, Utils.Color.CYAN) + "` in file : "
                    + Utils.color(file, Utils.Color.CYAN) + "\n");

            if (args.length > 2) {
                repeat = args[2].equals("Y") ? true : false;
            }

            if (Utils.isRegEx(pattern)) {
                Automaton.automaton(file, pattern, true);
            } else if (Utils.isAlpha(pattern)) {
                RadixTree.radixtree(file, pattern, repeat);
            } else {
                Kmp.kmp(file, pattern);
            }
            System.out.println(Utils.color("Bye Bye !", Color.CYAN));
        } else {
            System.out.println("Please provide a pattern and a valid file location");
        }
    }

}