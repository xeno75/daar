package main.kmp;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import main.Utils;
import main.Utils.Color;

public class Kmp {

	public static void kmp(String file, String pattern) {

		System.out.println(Utils.color("*** Processing with Kmp ***", Utils.Color.GREEN) + "\n");
		Path path = Paths.get(file);
		BufferedReader reader;
		try {
			reader = Files.newBufferedReader(path);
			String line = "";
			char[] patt = pattern.toCharArray();
			int nbrLines = 0;
			long startTime = System.currentTimeMillis();
			int gap = (Utils.RED.length() + Utils.RESETCOLOR.length());
			StringBuilder matchedLines = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				List<Integer> posis = match(patt, chercherRetenu(patt), line.toCharArray());
				StringBuilder builder = new StringBuilder();
				builder.append(line);
				if (!posis.isEmpty()) {
					int next = 0;
					int i = 1;
					for (Integer pos : posis) {
						builder.insert(next + pos, Utils.RED);
						next = next + pos + Utils.RED.length() + patt.length;
						builder.insert(next, Utils.RESETCOLOR);
						next = i++ * gap;
					}
					nbrLines++;
					matchedLines.append(builder);
					matchedLines.append("\n");
				}

			}

			System.out.println(matchedLines.toString());
			long endTime = System.currentTimeMillis();

			System.out.println(Utils.color("Number of matched lines : " + nbrLines, Utils.Color.GREEN));
			System.out.println(Utils.color("Time : " + (endTime - startTime) + " ms", Utils.Color.GREEN));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static int[] chercherRetenu(char[] patt) {
		int[] retenu = new int[patt.length + 1];
		retenu[0] = -1;
		int i = 0;
		try {
			for (int j = 1; j < patt.length; j++) {
				while (i > 0 && patt[i] != patt[j])
					i = retenu[i];

				if (patt[i] == patt[j])
					++i;

				if (j < patt.length - 1 && patt[j + 1] == patt[i]) {
					retenu[j + 1] = 0;
				} else {
					retenu[j + 1] = i;
				}
				if (patt[j] == patt[0]) {
					retenu[j] = -1;
				}
			}
		} catch (Exception e) {
			System.out.println(Utils.color("*** Pattern invalid for KMP ***", Color.RED));
		}
		return retenu;
	}

	public static List<Integer> match(char[] facteur, int[] retenue, char[] texte) {

		int i = 0;
		int j = 0;
		ArrayList<Integer> posis = new ArrayList<>();

		while (i < texte.length) {

			if (j == facteur.length) {
				posis.add(i - facteur.length);
				j = 0;
			}

			if (facteur[j] == texte[i]) {
				i++;
				j++;
			} else {
				if (retenue[j] == -1) {
					i++;
					j = 0;
				} else {
					j = retenue[j];
				}
			}
		}

		return posis;
	}

}
