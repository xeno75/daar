package main.radixtree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import main.*;

public class Node implements Serializable {

    private static final long serialVersionUID = 6027899523741335849L;

    public boolean[] flag;
    public Node[] childrends;
    public Set<Position> coords = new HashSet<>();

    public Node() {
        this.flag = new boolean[52];
        this.childrends = new Node[52];
        for (int i = 0; i < 52; i++) {
            this.flag[i] = false;
            this.childrends[i] = null;
        }
    }

    public void insert(String s, ArrayList<Position> posis, int c) {

        if (s.length() == 0) {
            this.flag[c] = true;
            this.coords.addAll(posis);
        }

        if (s.length() != 0) {
            int idx = Utils.charToint(s.charAt(0));
            if (this.childrends[idx] == null)
                this.childrends[idx] = new Node();
            this.childrends[idx].insert(s.substring(1), posis, idx);
        }

    }

    public List<Position> getPositions() {

        for (Node child : childrends) {
            if (child != null)
                coords.addAll(child.getPositions());
        }

        return new ArrayList<>(coords);

    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < childrends.length; i++) {
            if (childrends[i] != null)
                s += (char) (i + (int) 'a');
        }
        return s;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Node n = new Node();
        n.childrends = childrends.clone();
        n.flag = flag.clone();
        return n;
    }

}
