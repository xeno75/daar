package main.radixtree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import main.*;
import main.Utils.Color;
import main.kmp.Kmp;

public class RadixTree implements Serializable {

    private static final long serialVersionUID = -9052956009316647326L;

    public Node root;

    public static int index = 0;
    public static int MAX_VALUE = 100;
    public static Map<String, WordStat> map_occur = new HashMap<>();
    public static Set<String> blackList = new HashSet<>();

    public RadixTree() {
        this.root = new Node();
    }

    public static void radixtree(String src, String pattern, boolean samefile) {

        System.out.println(Utils.color("*** Processing with Radixtree ***", Utils.Color.GREEN) + "\n");
        long startTime = System.currentTimeMillis();
        constructMap(src);
        sortDESC();

        String cache = "caches/" + src + ".cache";
        File tmpDirC = new File(cache);
        boolean existsCache = tmpDirC.exists();

        // Don't use desrialize very slow
        // File tmpDirS = new File(cache + ".ser");
        // boolean existsCacheSer = tmpDirS.exists();

        RadixTree tree = new RadixTree();

        if (!existsCache /* || !existsCacheSer */) {
            System.out.println("***" + Utils.color(src + " not cached", Color.RED) + "***");
            writeCache(maPtoString(map_occur), cache);
        } else {
            System.out.println("***" + Utils.color(src + " cached", Color.GREEN) + "***");
        }

        tree.fillTree(cache);

        long endTime = System.currentTimeMillis();

        System.out.println(
                Utils.color("Time to read cache: " + (endTime - startTime) + " ms", Utils.Color.YELLOW) + "\n");

        // Don't use desrialize very slow
        // if (!existsCacheSer) {
        // if (existsCache)
        // serialize(cache + ".ser", tree);
        // } else {
        // tree = deserialize(cache + ".ser");
        // }

        String regEx = pattern;
        Scanner sc = new Scanner(System.in);

        do {
            Path path = Paths.get(src);
            BufferedReader reader;
            String line;
            StringBuilder matchedLines = new StringBuilder();
            long startTimeFind = System.currentTimeMillis();
            int nbrLines = 0;
            int idLine = 0;
            try {
                List<Position> posis = tree.findWord(regEx);
                reader = Files.newBufferedReader(path);

                while ((line = reader.readLine()) != null) {
                    idLine++;
                    StringBuilder matchedString = new StringBuilder();
                    for (Position pos : posis) {
                        int col = pos.col;
                        int lig = pos.lig;
                        if (idLine == lig) {
                            nbrLines++;
                            matchedString.append(line.substring(0, col));
                            matchedString
                                    .append(Utils.RED + line.substring(col, col + regEx.length()) + Utils.RESETCOLOR);
                            matchedString.append(line.substring(col + regEx.length()));
                            matchedLines.append(matchedString);
                            matchedLines.append("\n");
                            break;
                        }
                    }

                }

                long endTimeFind = System.currentTimeMillis();
                if (nbrLines > 0) {
                    System.out.println(matchedLines.toString());
                    System.out.println(Utils.color("Number of matched lines : " + nbrLines, Utils.Color.GREEN));
                    System.out
                            .println(Utils.color("Time : " + (endTimeFind - startTimeFind) + " ms", Utils.Color.GREEN));

                } else {
                    System.out.println("\n" + Utils.color("*** Nothing found with RadixTree ***", Utils.Color.RED));
                    Kmp.kmp(src, regEx);
                }

                if (samefile) {
                    System.out.println("Insert another pattern or abort (type q) !");
                    String insrted = sc.nextLine();
                    if (insrted.equals("q")) {
                        break;
                    } else {
                        regEx = insrted;
                        System.out.println("\n" + Utils.color("*** Processing with Radixtree ***", Color.GREEN) + "\n");
                    }
                }

            } catch (IOException | CloneNotSupportedException e) {
                System.out.println(e.getMessage());
            }

        } while (samefile);

        sc.close();

    }

    // unused because of condition length(word) > 4
    public void init_hashSet() {
        String blk = "he is me to the txt";
        for (String s : blk.split(" ")) {
            blackList.add(s);
        }
    }

    public void printTree(Node node) {
        if (node == null) {
            return;
        }
        System.out.println("[");
        System.out.print(node);
        for (int i = 0; i < node.childrends.length; i++) {
            if (node.childrends[i] != null) {
                System.out.print("--" + Utils.intToChar(i) + "--");
            } else {
                continue;
            }
            this.printTree(node.childrends[i]);
            System.out.print("-end-" + Utils.intToChar(i) + "--");
        }
        System.out.println("]");
    }

    public void fillTree(String path) {
        Path p = Paths.get(path);
        BufferedReader reader;
        try {
            reader = Files.newBufferedReader(p);

            String line;
            while ((line = reader.readLine()) != null) {
                String[] coords = line.split(" ");
                ArrayList<Position> posis = new ArrayList<>();
                String word = coords[0];
                for (int i = 2; i < coords.length - 1; i += 2) {
                    int col = Integer.parseInt(coords[i]);
                    int lig = Integer.parseInt(coords[i + 1]);
                    Position pos = new Position(col, lig, word.length());
                    posis.add(pos);
                }
                this.root.insert(word, posis, Utils.charToint(word.charAt(0)));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Position> findWord(String s) throws CloneNotSupportedException {
        Node current = root;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int idx = Utils.charToint(c);
            if (current.childrends[idx] == null) {
                return new ArrayList<>();
            }
            current = current.childrends[idx];
        }

        int idxFlag = Utils.charToint(s.charAt(s.length() - 1));
        if (current.flag[idxFlag]) {
            ArrayList<Position> lst = new ArrayList<>();
            lst.addAll(current.getPositions());
            return lst;
        } else {
            return new ArrayList<>();
        }
    }

    private static void count_occur(String key, int colonne, int ligne) {
        if (map_occur.containsKey(key)) {
            map_occur.get(key).add(new Integer[] { colonne, ligne });
        } else {
            WordStat stat = new WordStat();
            stat.add(new Integer[] { colonne, ligne });
            map_occur.put(key, stat);
        }
        if (map_occur.get(key).size() > MAX_VALUE) {
            blackList.add(key);
            map_occur.remove(key);
        }
    }

    private static void sortDESC() {
        map_occur = map_occur.entrySet().parallelStream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o, n) -> o, LinkedHashMap::new));
    }

    private static String maPtoString(Map<String, WordStat> map) {
        StringBuilder sBuilder = new StringBuilder();
        for (String word : map.keySet()) {
            sBuilder.append(word + " " + map.get(word).size());
            for (Integer[] coord : map.get(word).coord) {
                sBuilder.append(" ");
                // sBuilder.append("(");
                sBuilder.append(coord[0]);
                // comment if no need to coord
                sBuilder.append(" ");
                sBuilder.append(coord[1]);
                // sBuilder.append(")");
                // ----------------

            }
            sBuilder.append("\n");
        }
        return sBuilder.toString();
    }

    private static void constructMap(String src) {
        Path path = Paths.get(src);
        BufferedReader reader;
        try {
            reader = Files.newBufferedReader(path);
            String line;
            int idLine = 0;
            int colonne;
            while ((line = reader.readLine()) != null) {
                idLine++;
                colonne = 0;
                String[] words = line.split("[^A-Za-z]");
                for (String word : words) {
                    colonne = line.indexOf(word);
                    if (word.length() > 4 && !blackList.contains(word))
                        count_occur(word, colonne, idLine);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void writeCache(String content, String file) {
        try {
            Files.write(Paths.get(file), content.getBytes());
        } catch (IOException e) {
            System.out.println("IO error at writeCache function in radixtree " + e.getMessage());
        }
    }

    public static void serialize(String filename, RadixTree tree) {

        try {

            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            // write object
            out.writeObject(tree);

            out.close();
            file.close();

            System.out.println("Object has been serialized\n");

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("IOException is caught in serialize");
        }
    }

    public static RadixTree deserialize(String filename) {

        RadixTree tree = null;

        try {

            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            // Read object
            tree = (RadixTree) in.readObject();

            in.close();
            file.close();
            System.out.println("Object has been deserialized");

        }

        catch (IOException ex) {
            ex.printStackTrace();
            // System.out.println("IOException is caught in deserialize");
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException is caught");
        }

        return tree;
    }

}
