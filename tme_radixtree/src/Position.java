import java.io.Serializable;

/**
 * Position
 */
public class Position implements Serializable {

    private static final long serialVersionUID = -4011627828472004305L;

    int col;
    int lig;
    int length;

    public Position(int col, int lig, int length) {
        this.col = col;
        this.lig = lig;
        this.length = length;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Position(col, lig, length);
    }

    @Override
    public String toString() {
        return "[col=" + col + ", length=" + length + ", lig=" + lig + "]";
    }

}