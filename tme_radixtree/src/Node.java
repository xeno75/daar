import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Node implements Serializable {

    private static final long serialVersionUID = 6027899523741335849L;

    public boolean[] flag;
    public Node[] childrends;
    public List<Position> coords = new ArrayList<>();

    public Node() {
        this.flag = new boolean[52];
        this.childrends = new Node[52];
        for (int i = 0; i < 52; i++) {
            this.flag[i] = false;
            this.childrends[i] = null;
        }
    }

    public void insert(String s, ArrayList<Position> posis) {

        if (s.length() == 1) {
            int idx;
            if ((int) s.charAt(0) < 97) {
                idx = RadixTree.charTointMaj(s.charAt(0));
            } else {
                idx = RadixTree.charToint(s.charAt(0));
            }
            this.flag[idx] = true;
            this.coords.addAll(posis);
        }

        if (s.length() != 0) {
            int idx;
            if ((int) s.charAt(0) < 97) {
                idx = RadixTree.charTointMaj(s.charAt(0));
            } else {
                idx = RadixTree.charToint(s.charAt(0));
            }
            if (this.childrends[idx] == null)
                this.childrends[idx] = new Node();
            this.childrends[idx].insert(s.substring(1), posis);
        }

    }

    public List<Position> getPositions() {

        for (Node child : childrends) {
            if (child != null)
                coords.addAll(child.getPositions());
        }

        return coords;

    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < childrends.length; i++) {
            if (childrends[i] != null)
                s += (char) (i + (int) 'a');
        }
        return s;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Node n = new Node();
        n.childrends = childrends.clone();
        n.flag = flag.clone();
        return n;
    }

}
