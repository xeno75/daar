import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class RadixTree implements Serializable {

    private static final long serialVersionUID = 3291574718694945730L;

    public Node root;

    public RadixTree() {
        this.root = new Node();
    }

    public void printTree(Node node) {
        if (node == null) {
            return;
        }
        System.out.println("[");
        System.out.print(node);
        for (int i = 0; i < node.childrends.length; i++) {
            if (node.childrends[i] != null) {
                System.out.print("--" + intToChar(i) + "--");
            } else {
                continue;
            }
            this.printTree(node.childrends[i]);
            System.out.print("-end-" + intToChar(i) + "--");
        }
        System.out.println("]");
    }

    public static int charToint(char c) {
        return (int) c - (int) 'a';
    }

    public static int charTointMaj(char c) {
        return (int) c - (int) 'A' + 26;
    }

    public static char intToChar(int i) {
        return (char) (i + (int) 'a');
    }

    public void fillTree(String path) throws IOException {
        Path p = Paths.get(path);
        BufferedReader reader = Files.newBufferedReader(p);
        String line;
        while ((line = reader.readLine()) != null) {
            String[] coords = line.split(" ");
            ArrayList<Position> posis = new ArrayList<>();
            String word = coords[0];
            for (int i = 2; i < coords.length - 1; i += 2) {
                int col = Integer.parseInt(coords[i]);
                int lig = Integer.parseInt(coords[i + 1]);
                Position pos = new Position(col, lig, word.length());
                posis.add(pos);
            }
            this.root.insert(word, posis);
        }
    }

    public List<Position> findWord(String s) throws CloneNotSupportedException {
        // s = s.toLowerCase();
        Node current = root;
        for (int i = 0; i < s.length() - 1; i++) {
            char c = s.charAt(i);
            int idx;
            if ((int) c < 97) {
                idx = charTointMaj(c);
            } else {
                idx = charToint(c);
            }
            if (current.childrends[idx] == null) {
                return new ArrayList<>();
            }
            current = current.childrends[idx];
        }

        int idxFlag;
        if ((int) s.charAt(s.length() - 1) < 97) {
            idxFlag = charTointMaj(s.charAt(s.length() - 1));
        } else {
            idxFlag = charToint(s.charAt(s.length() - 1));
        }

        if (current.flag[idxFlag]) {
            return current.getPositions();
        } else {
            return new ArrayList<>();
        }
    }

}
