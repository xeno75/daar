import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

public class Matcher {

    public static int index = 0;
    public static int MAX_VALUE = 100;

    final static String RED = (char) 27 + "[31;1m";
    final static String REDBG = (char) 27 + "[41;1m";
    final static String RESETCOLOR = (char) 27 + "[0m";

    Map<String, WordStat> map_occur = new HashMap<>();
    Set<String> blackList = new HashSet<>();

    // unused because of condition length(word) > 4
    public void init_hashSet() {
        String blk = "he is me to the txt";
        for (String s : blk.split(" ")) {
            blackList.add(s);
        }
    }

    public static void main(String... args) throws Exception {

        // String fileName = "babylon";

        // Matcher m = new Matcher();

        // m.constructMap(fileName + ".txt");
        // m.sortDESC();

        // String cache = fileName + ".cache";

        // writeCache(m.maPtoString(m.map_occur), cache);

        // RadixTree tree = new RadixTree();
        // tree.fillTree(cache);

        // Test serialize : ✓
        // serialize(cache + ".ser", tree);

        // // Test deserialize : ✓
        // RadixTree tree = new RadixTree();
        // tree = deserialize(cache + ".ser");

        // System.out.println("Match the word : poulete");
        // System.out.println("Word matched ? " + !tree.findWord("poulete").isEmpty());

        // System.out.println("Match the word : Sargon");
        // System.out.println("Word matched ? " + !tree.findWord("Sargon").isEmpty());
        // List<Position> posis = tree.findWord("Sargon");
        // System.out.println(posis.size());

        // System.out.println("Match the word : Sargonids");
        // System.out.println("Word matched ? " +
        // !tree.findWord("Sargonids").isEmpty());
        // System.out.println(tree.findWord("Sargonids"));

        // // Fail ? ==> Correct ! it's not a fail --> occur(babylon) > maxvalue
        // System.out.println("Match the word : Babylon");
        // System.out.println("Word matched ? " + !tree.findWord("Babylon").isEmpty());

        testFile("babylon");

    }

    private void count_occur(String key, int colonne, int ligne) {
        if (this.map_occur.containsKey(key)) {
            this.map_occur.get(key).add(new Integer[] { colonne, ligne });
        } else {
            WordStat stat = new WordStat();
            stat.add(new Integer[] { colonne, ligne });
            this.map_occur.put(key, stat);
        }
        if (this.map_occur.get(key).size() > MAX_VALUE) {
            this.blackList.add(key);
            this.map_occur.remove(key);
        }
    }

    private void sortDESC() {
        map_occur = map_occur.entrySet().parallelStream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o, n) -> o, LinkedHashMap::new));
    }

    private String maPtoString(Map<String, WordStat> map) {
        StringBuilder sBuilder = new StringBuilder();
        for (String word : map.keySet()) {
            sBuilder.append(word + " " + map.get(word).size());
            for (Integer[] coord : map.get(word).coord) {
                sBuilder.append(" ");
                // sBuilder.append("(");
                sBuilder.append(coord[0]);
                // comment if no need to coord
                sBuilder.append(" ");
                sBuilder.append(coord[1]);
                // sBuilder.append(")");
                // ----------------

            }
            sBuilder.append("\n");
        }
        return sBuilder.toString();
    }

    private void constructMap(String src) throws Exception {
        Path path = Paths.get(src);
        BufferedReader reader = Files.newBufferedReader(path);
        String line;
        int idLine = 0;
        int colonne;
        while ((line = reader.readLine()) != null) {
            idLine++;
            colonne = 0;
            String[] words = line.split("[^A-Za-z]");
            for (String word : words) {
                colonne = line.indexOf(word);
                // word = word.toLowerCase();
                if (word.length() > 4 && !this.blackList.contains(word))
                    count_occur(word, colonne, idLine);
            }
        }
    }

    private static void writeCache(String content, String dest) throws IOException {
        Files.write(Paths.get(dest), content.getBytes());
    }

    public static void serialize(String filename, RadixTree tree) {

        try {

            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            // write object
            out.writeObject(tree);

            out.close();
            file.close();

            System.out.println("Object has been serialized\n");

        } catch (IOException ex) {
            System.out.println("IOException is caught in serialize");
        }
    }

    public static RadixTree deserialize(String filename) {

        RadixTree tree = null;

        try {

            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            // Read object
            tree = (RadixTree) in.readObject();

            in.close();
            file.close();
            System.out.println("Object has been deserialized");

        }

        catch (IOException ex) {
            System.out.println("IOException is caught in deserialize");
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException is caught");
        }

        return tree;
    }

    private static void testFile(String src) throws Exception {

        Matcher m = new Matcher();
        m.constructMap(src + ".txt");
        m.sortDESC();
        String cache = src + ".cache";
        writeCache(m.maPtoString(m.map_occur), cache);
        RadixTree tree = new RadixTree();
        tree.fillTree(cache);
        Path path = Paths.get(src + ".txt");
        BufferedReader reader = Files.newBufferedReader(path);
        String line;
        String regEx = "Sargon";
        int idLine = 0;
        List<Position> posis = tree.findWord(regEx);
        while ((line = reader.readLine()) != null) {
            idLine++;
            StringBuilder matchedString = new StringBuilder();
            for (Position pos : posis) {
                int col = pos.col;
                int lig = pos.lig;
                if (idLine == lig) {
                    matchedString.append(line.substring(0, col));
                    matchedString.append(RED + line.substring(col, col + regEx.length()) + RESETCOLOR);
                    matchedString.append(line.substring(col + regEx.length()));
                    System.out.println(matchedString);
                    break;
                }
            }
        }
    }
}