import java.util.ArrayList;
import java.io.Serializable;
import java.lang.Comparable;

/**
 * WordStat
 */
public class WordStat implements Comparable<WordStat>, Serializable {

    private static final long serialVersionUID = 555106709403203606L;
    
    public ArrayList<Integer[]> coord = new ArrayList<>();

    public int size() {
        return coord.size();
    }

    public void add(Integer[] value) {
        this.coord.add(value);
    }

    @Override
    public int compareTo(WordStat o) {
        return Integer.compare(((WordStat) o).size(),this.size());
    }
}