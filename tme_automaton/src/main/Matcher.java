package main;

import java.util.ArrayList;
import java.util.List;
import main.RegExTree;

/**
 * Utils : funct addEpsilonTrans, funct addTrans
 */
public class Matcher {

    public static int label = 0;

    private void addEpsilonTrans(State from, State to) {
        from.getEpsilonTrans().add(to);
    }

    private void addTrans(State from, State to, int token) {
        from.getTransitions().put(token, to);
    }

    private Automaton processToken(int token) {
        State start = new State(false);
        State end = new State(true);

        addTrans(start, end, token);

        return new Automaton(start, end);
    }

    private Automaton processDot() {
        State start = new State(false);
        State end = new State(true);

        for (int token = 0; token < 128; token++) {
            addTrans(start, end, token);
        }

        return new Automaton(start, end);
    }

    private Automaton processAltern(Automaton first, Automaton second) {

        State start = new State(false);
        State end = new State(true);

        addEpsilonTrans(start, first.getStart());
        addEpsilonTrans(start, second.getStart());

        addEpsilonTrans(first.getEnd(), end);
        addEpsilonTrans(second.getEnd(), end);

        first.getEnd().setIsEnd(false);
        second.getEnd().setIsEnd(false);

        return new Automaton(start, end);

    }

    private Automaton processConcat(Automaton first, Automaton second) {

        addEpsilonTrans(first.getEnd(), second.getFirst());

        first.getEnd().setIsEnd(false);

        return new Automaton(first.getStart(), second.getEnd());
    }

    private Automaton processClosure(Automaton automaton) {

        State start = new State(false);
        State end = new State(true);

        addEpsilonTrans(automaton.getEnd(), automaton.getStart());
        addEpsilonTrans(automaton.getEnd(), end);
        addEpsilonTrans(start, automaton.getStart());
        addEpsilonTrans(start, end);

        automaton.getEnd().setIsEnd(false);

        return new Automaton(start, end);

    }

    private Automaton processPlus(Automaton automaton) {

        State start = new State(false);
        State end = new State(true);

        addEpsilonTrans(automaton.getEnd(), automaton.getStart());
        addEpsilonTrans(automaton.getEnd(), end);
        addEpsilonTrans(start, automaton.getStart());

        automaton.getEnd().setIsEnd(false);

        return new Automaton(start, end);
    }

    public Automaton mergeAutomaton(RegExTree tree) {

        if (tree.root == RegEx.CONCAT)
            return processConcat(mergeAutomaton(tree.subTrees.get(0)), mergeAutomaton(tree.subTrees.get(1)));
        if (tree.root == RegEx.ETOILE)
            return processClosure(mergeAutomaton(tree.subTrees.get(0)));
        if (tree.root == RegEx.ALTERN)
            return processAltern(mergeAutomaton(tree.subTrees.get(0)), mergeAutomaton(tree.subTrees.get(1)));
        if (tree.root == RegEx.PLUS)
            return processPlus(mergeAutomaton(tree.subTrees.get(0)));
        if (tree.root == RegEx.DOT)
            return processDot();

        return processToken(tree.root);

    }

    /**
     * Convert the automaton to an automaton without epsilon-transition
     */
    private void processAutomaton(State start, List<State> lstStates, List<State> visited) {
        if (!start.getEpsilonTrans().isEmpty()) {
            for (State s : start.getEpsilonTrans()) {
                if (!visited.contains(s)) {
                    visited.add(s);
                    processAutomaton(s, lstStates, visited);
                }
            }
        } else {
            lstStates.add(start);
        }
    }

    public List<Position> match(String txt, String regEx) throws Exception {

        Automaton a = mergeAutomaton(RegEx.parse(regEx));
        List<State> lstStates = new ArrayList<>();
        processAutomaton(a.getStart(), lstStates, new ArrayList<>());
        char[] chars = txt.toCharArray();
        int index = 0;
        boolean existsTransition;
        List<Position> indexs = new ArrayList<>();

        ret: while (index < chars.length) {
            existsTransition = false;
            List<State> lstNStates = new ArrayList<>();
            int c = chars[index];
            Position p = new Position();
            for (int i = 0; i < lstStates.size(); i++) {
                State st = lstStates.get(i);
                State nst = st.getTransitions().get(c);
                if (nst != null) {
                    processAutomaton(nst, lstNStates, new ArrayList<>());
                    existsTransition = true;
                    p.col = index;
                }
            }

            if (!existsTransition) {
                index++;
                lstStates.clear();
                processAutomaton(a.getStart(), lstStates, new ArrayList<>());
                p.col = 0;
                continue ret;
            }

            lstStates = lstNStates;
            for (State st : lstStates) {
                if (st.isEnd()) {
                    p.lig = index + 1;
                    if (!indexs.contains(p))
                        indexs.add(p);
                }
            }
            index++;
            processAutomaton(a.getStart(), lstStates, new ArrayList<>());
        }
        return indexs;
    }

    public boolean match(Automaton a, String txt) {

        List<State> lstStates = new ArrayList<>();
        processAutomaton(a.getStart(), lstStates, new ArrayList<>());
        for (char c : txt.toCharArray()) {
            List<State> lstNStates = new ArrayList<>();
            for (State st : lstStates) {
                State nst = st.getTransitions().get((int) c);
                if (nst != null) {
                    processAutomaton(nst, lstNStates, new ArrayList<>());
                }
            }
            lstStates = lstNStates;
        }

        for (State st : lstStates) {
            if (st.isEnd()) {
                return true;
            }
        }
        return false;
    }

    public boolean isIn(String regEx, char c) {
        for (char ch : regEx.toCharArray()) {
            if (ch == c) {
                return true;
            } else if (ch == '.') {
                return true;
            }
        }
        return false;
    }

    public String show(RegExTree tree) {
        String s = "";
        if (tree.subTrees.isEmpty()) {
            s += rootToString(tree.root);
        } else {
            s += rootToString(tree.root);
            for (RegExTree intree : tree.subTrees) {
                s += show(intree);
            }
        }
        return s;
    }

    private String rootToString(int root) {
        if (root == RegEx.CONCAT)
            return ".";
        if (root == RegEx.ETOILE)
            return "*";
        if (root == RegEx.PLUS)
            return "+";
        if (root == RegEx.ALTERN)
            return "|";
        if (root == RegEx.DOT)
            return ".";
        return Character.toString((char) root);
    }
}