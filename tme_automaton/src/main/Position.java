package main;

/**
 * Position
 */
public class Position {

    int col;
    int lig;
    int length;

    public Position() {

    }

    public Position(int col, int lig, int length) {
        this.col = col;
        this.lig = lig;
        this.length = length;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Position(col, lig, length);
    }

    @Override
    public String toString() {
        return "[col=" + col + ", length=" + length + ", lig=" + lig + "]";
    }

    @Override
    public boolean equals(Object arg0) {
        Position p = (Position) arg0;
        return this.col == p.col && this.lig == p.lig && this.length == p.length;
    }

}