package main;

/**
 * Automaton
 */
public class Automaton extends Tuple<State> {

    public Automaton(State first, State second) {
        super(first, second);
    }

    public State getStart() {
        return getFirst();
    }

    public State getEnd() {
        return getSecond();
    }
        
}