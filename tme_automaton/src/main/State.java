package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * State
 */
public class State {

    private int label = Matcher.label++;
    private boolean isEnd;
    private Map<Integer, State> transitions;
    private List<State> epsilonTrans;

    public State(boolean isEnd) {
        init(isEnd,new HashMap<Integer, State>(),new ArrayList<State>());
    }

    private void init(boolean isEnd, Map<Integer, State> transitions, List<State> epsilonTrans) {
        this.isEnd = isEnd;
        this.transitions = transitions;
        this.epsilonTrans = epsilonTrans;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setIsEnd(boolean isEnd) {
        this.isEnd = isEnd;
    }

    public Map<Integer, State> getTransitions() {
        return transitions;
    }

    public void setTransitions(Map<Integer, State> transitions) {
        this.transitions = transitions;
    }

    public List<State> getEpsilonTrans() {
        return epsilonTrans;
    }

    public void setEpsilonTrans(List<State> epsilonTrans) {
        this.epsilonTrans = epsilonTrans;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return ""+label;
    }

    @Override
    public boolean equals(Object arg0) {
        return ((State) arg0).label == label;
    }

}