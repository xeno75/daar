package main;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Main
 */
public class Main {

    final static String RED = (char) 27 + "[31;1m";
    final static String REDBG = (char) 27 + "[41;1m";
    final static String RESETCOLOR = (char) 27 + "[0m";

    private static void readFile(String src) throws Exception {
        Path path = Paths.get(src);
        BufferedReader reader = Files.newBufferedReader(path);
        String line;
        int nbr = 0;
        StringBuilder matchedString = new StringBuilder();
        Matcher m = new Matcher();
        // String regEx = "(h|e|l|p)*";
        String regEx = "s(a|r|g)*.n";
        // String regEx = "S(g|a|(z|r)+|r)+.n";
        boolean matched = false;
        List<Position> matchedIndexs;
        while ((line = reader.readLine()) != null) {
            // for (String word : line.split(" ")) {
            matchedIndexs = m.match(line, regEx);
            Collections.sort(matchedIndexs, new Comparator<Position>() {

                @Override
                public int compare(Position o1, Position o2) {
                    return Integer.compare(o1.col, o2.col);
                }
            });

            if (!matchedIndexs.isEmpty()) {
                int idx = 0;
                for (Position pos : matchedIndexs) {
                    matchedString.append(line.substring(idx, pos.col));
                    matchedString.append(Main.RED + line.substring(pos.col, pos.lig) + Main.RESETCOLOR);
                    // matchedString.append(line.substring(pos.lig));
                    nbr++;
                    idx = pos.lig;
                    matched = true;
                }
            }

            if (matched)
                System.out.println(matchedString.toString());

            matchedString = new StringBuilder();
            matched = false;
        }
        // System.out.println("Nombre de fois trouvé : " + nbr);
    }

    public static void main(String[] args) throws Exception {
        readFile("babylon.txt");
        // Matcher m = new Matcher();
        // // String regEx = "s(a|r|g)*.n";
        // String regEx = "(h|e|l|p)";// ".ar.|s..s";/* "a|b+|org"; */ // "h+|a.";
        // System.out.println(RegEx.parse(regEx));
        // String phrase = "hdjdj sandjdkd";
        // System.out.println(m.match(phrase, regEx));
        // for (int idx : m.match(phrase, regEx)) {
        // System.out.println((Main.RED + phrase.substring(0, idx + 1) + Main.RESETCOLOR
        // + phrase.substring(idx + 1))) ;
        // }

    }

}