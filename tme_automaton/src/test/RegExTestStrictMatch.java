package test;

import main.Automaton;
import main.Matcher;
import main.RegEx;
import main.RegExTree;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 * RegExTest for strict match
 */
public class RegExTestStrictMatch {

    Matcher m;
    Automaton dfa;
    RegExTree tree;

    @Before
    public void setUp() {
        m = new Matcher();
    }

    @Test
    public void testConcat() throws Exception {
        tree = RegEx.parse("ab");
        dfa = m.mergeAutomaton(tree);
        assertTrue(m.match(dfa, "ab"));
        assertFalse(m.match(dfa, "acb"));
    }

    @Test
    public void testAltern() throws Exception {
        tree = RegEx.parse("a|b");
        dfa = m.mergeAutomaton(tree);
        assertTrue(m.match(dfa, "a"));
        assertTrue(m.match(dfa, "b"));
        assertFalse(m.match(dfa, "c"));
    }

    @Test
    public void testEtoile() throws Exception {
        tree = RegEx.parse("a*");
        dfa = m.mergeAutomaton(tree);
        assertTrue(m.match(dfa, ""));
        assertTrue(m.match(dfa, "a"));
        assertTrue(m.match(dfa, "aaaaaaaaa"));
        assertFalse(m.match(dfa, "z"));
    }

    @Test
    public void testPlus() throws Exception {
        tree = RegEx.parse("a+");
        dfa = m.mergeAutomaton(tree);
        assertTrue(m.match(dfa, "a"));
        assertTrue(m.match(dfa, "aaaaaaaaa"));
        assertFalse(m.match(dfa, ""));
    }

    @Test
    public void testRegEx() throws Exception {
        tree = RegEx.parse("(a|b(c|d)+)+");
        dfa = m.mergeAutomaton(tree);
        assertTrue(m.match(dfa, "a"));
        assertTrue(m.match(dfa, "bc"));
        assertTrue(m.match(dfa, "bcccccc"));
        assertTrue(m.match(dfa, "abcd"));
        assertTrue(m.match(dfa, "bd"));
        assertFalse(m.match(dfa, "b"));
        assertFalse(m.match(dfa, "bzc"));

        tree = RegEx.parse("S(g|r|a)+on");
        dfa = m.mergeAutomaton(tree);
        assertTrue(m.match(dfa, "Sgggaraaagaaaggon"));
        assertFalse(m.match(dfa, "Sagro"));
    }

}