import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Matcher {

	/**
	 * comparer notre egrep à celui de linux si egrep va 5 à 10 fois plus vite c'est
	 * bon ( le prof s'attend à un facteur entre 100 et 1000 )
	 */

	final static String RED = (char) 27 + "[31;1m";
	final static String REDBG = (char) 27 + "[41;1m";
	final static String RESETCOLOR = (char) 27 + "[0m";

	public static List<Integer> match(char[] facteur, int[] retenue, char[] texte) {

		int i = 0;
		int j = 0;
		ArrayList<Integer> posis = new ArrayList<>();

		while (i < texte.length) {

			if (j == facteur.length) {
				posis.add(i - facteur.length);
				j = 0;
			}

			if (facteur[j] == texte[i]) {
				i++;
				j++;
			} else {
				if (retenue[j] == -1) {
					i++;
					j = 0;
				} else {
					j = retenue[j];
				}
			}
		}

		return posis;
	}

	public static void readFile() throws IOException {
		Path path = Paths.get("testkmp.txt");
		long startTime = System.currentTimeMillis();
		BufferedReader reader = Files.newBufferedReader(path);
		String line = "";
		char[] patt = "ocoo".toCharArray();
		int idLine = 0;
		StringBuilder builder = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			List<Integer> posis = Matcher.match(patt, chercherRetenu(patt), line.toCharArray());
			builder.append(line);
			if (!posis.isEmpty()) {
				int next = 0;
				int i = 1;
				for (Integer pos : posis) {
					builder.insert(next + pos, RED);
					next = next + pos + RED.length() + patt.length;
					builder.insert(next, RESETCOLOR);
					next = i++ * (RED.length() + RESETCOLOR.length());
				}
				idLine++;
				System.out.println(builder.toString());
			}

		}
		long endTime = System.currentTimeMillis();

		System.out.println(RED + chercherRetenu(patt) + RESETCOLOR);

		System.out.println("Nombre de lignes matchées : " + idLine);
		System.out.println("Time : " + (endTime - startTime));

	}

	// public static int[] chercherRetenu(char[] patt) {
	// 	int[] retenu = new int[patt.length + 1];
	// 	retenu[0] = -1;
	// 	int i = 0;
	// 	for (int j = 1; j < patt.length; j++) {
	// 		while (i > 0 && patt[i] != patt[j])
	// 			i = retenu[i];

	// 		if (patt[i] == patt[j])
	// 			++i;

	// 		if (j < patt.length - 1 && patt[j + 1] == patt[i]) {
	// 			retenu[j + 1] = 0;
	// 		} else {
	// 			retenu[j + 1] = i;
	// 		}
	// 		if (patt[j] == patt[0]) {
	// 			retenu[j] = -1;
	// 		}
	// 	}
	// 	return retenu;
	// }

	public static int[] chercherRetenu(char[] patt) {
		int[] retenu = new int[patt.length + 1];
		retenu[0] = -1;
		int i = 0;
		for (int j = 1; j < patt.length; j++) {
			while (i > 0 && patt[i] != patt[j])
				i = retenu[i];

			if (patt[i] == patt[j])
				++i;

			if (j < patt.length - 1 && patt[j + 1] == patt[i]) {
				retenu[j + 1] = 0;
			} else {
				retenu[j + 1] = i;
			}
			if (patt[j] == patt[0]) {
				retenu[j] = -1;
			}
		}
		return retenu;
	}


	public static void main(String[] args) throws IOException {

		readFile();
		for (int i : chercherRetenu("ocoo".toCharArray())) {
			System.out.print(RED + i + RESETCOLOR);
		}

	}

}
