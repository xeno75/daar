

# 20 min = 1200 sec et 25 min = 1500 sec d'où les 1200 et 1500 dans le commande;
zless rollernet.dyn.gz | awk '{if ($3 >= 1200 && $3 <= 1500) { if ($1 < $2) print $1" "$2; else print $2" "$1;}}' | awk '!nbOccurences[$0]++;' > 2025.nodup

# awk a un option génial qui est l'option -F pour séparer en lui dennant la liste de tout les séparateur possible (qui nous intéresse)
# si non le séparateur par défault pour awk c'est " " (espace)