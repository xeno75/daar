import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Main
 */
public class Main {

    public static void main(String args[]) {
        Floydwarshall.print(Floydwarshall.calculShortestPaths(fileToEdges("2025.nodup")));
    }

    public static Set<Edge> fileToEdges(String fileName) {
        Set<Edge> edges = new HashSet<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach((s) -> {
                String[] ss = s.split(" ", 2);
                edges.add(new Edge(Integer.parseInt(ss[0]), Integer.parseInt(ss[1])));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return edges;
    }

}