import java.util.Objects;

/**
 * Edge
 */
public class Edge {

    int x;
    int y;

    Edge(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int distance() {
        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        Edge e = (Edge) obj;
        return ((e.x == this.x && e.y == this.y) || (e.x == this.y && e.y == this.x));
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "[x=" + x + ", y=" + y + "]\n";
    }

}