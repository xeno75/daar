import java.util.ArrayList;
import java.util.Set;

/**
 * Floydwarshall
 */
public class Floydwarshall {

    static double[][] dist;
    static int[][] paths;

    public static int[][] calculShortestPaths(Set<Edge> edges) {
        int nodes = 62;
        dist = new double[nodes][nodes];
        paths = new int[nodes][nodes];
        for (int i = 0; i < dist.length; i++) {
            for (int j = 0; j < dist.length; j++) {
                if (edges.contains(new Edge(i, j))) {
                    dist[i][j] = 1;
                } else {
                    dist[i][j] = Double.POSITIVE_INFINITY;
                }
                paths[i][j] = j;
            }
        }

        for (int k = 0; k < dist.length; k++) {
            for (int i = 0; i < dist.length; i++) {
                for (int j = 0; j < dist.length; j++) {
                    if (dist[i][j] > dist[i][k] + dist[k][j]) {
                        dist[i][j] = dist[i][k] + dist[k][j];
                        paths[i][j] = paths[i][k];
                    }
                }
            }
        }

        return paths;
    }

    public static ArrayList<Integer> pathFinder(int u, int v, int[][] paths) {
        ArrayList<Integer> chemin = new ArrayList<>();
        if (paths[u][v] == -1) {
            return chemin;
        }
        chemin.add(u);
        while (u != v) {
            u = paths[u][v];
            chemin.add(u);
        }
        return chemin;
    }

    public static void print(int[][] ks) {

        for (int i = 0; i < ks.length; i++) {
            for (int j = 0; j < ks.length; j++) {
                System.out.print("\t" + ks[i][j]);
            }
            System.out.println();
        }

    }
}